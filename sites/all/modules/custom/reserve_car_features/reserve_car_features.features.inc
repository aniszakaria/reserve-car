<?php
/**
 * @file
 * reserve_car_features.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function reserve_car_features_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function reserve_car_features_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function reserve_car_features_node_info() {
  $items = array(
    'cartest' => array(
      'name' => t('Car Reservation'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Requester Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_workflow_default_workflows().
 */
function reserve_car_features_workflow_default_workflows() {
  $workflows = array();

  // Exported workflow: Reservation State
  $workflows['Reservation State'] = array(
    'name' => 'Reservation State',
    'tab_roles' => 4,
    'options' => 'a:4:{s:16:"comment_log_node";i:0;s:15:"comment_log_tab";i:1;s:13:"name_as_title";i:0;s:12:"watchdog_log";i:1;}',
    'states' => array(
      0 => array(
        'state' => '(creation)',
        'weight' => -50,
        'sysid' => 1,
        'status' => 1,
        'name' => 'Reservation State',
      ),
      1 => array(
        'state' => 'Completed Reservation',
        'weight' => 0,
        'sysid' => 0,
        'status' => 1,
        'name' => 'Reservation State',
      ),
      2 => array(
        'state' => 'Awaiting Balance Confirmation',
        'weight' => 0,
        'sysid' => 0,
        'status' => 1,
        'name' => 'Reservation State',
      ),
      3 => array(
        'state' => 'Balance Hours',
        'weight' => 0,
        'sysid' => 0,
        'status' => 1,
        'name' => 'Reservation State',
      ),
      4 => array(
        'state' => 'Awaiting Requester',
        'weight' => 0,
        'sysid' => 0,
        'status' => 1,
        'name' => 'Reservation State',
      ),
      5 => array(
        'state' => 'Awaiting Accountant ',
        'weight' => 0,
        'sysid' => 0,
        'status' => 1,
        'name' => 'Reservation State',
      ),
      6 => array(
        'state' => 'Requester Sent new Confirmation',
        'weight' => 0,
        'sysid' => 0,
        'status' => 1,
        'name' => 'Reservation State',
      ),
      7 => array(
        'state' => 'Supervisor',
        'weight' => 2,
        'sysid' => 0,
        'status' => 1,
        'name' => 'Reservation State',
      ),
      8 => array(
        'state' => 'Reservation Admin',
        'weight' => 2,
        'sysid' => 0,
        'status' => 1,
        'name' => 'Reservation State',
      ),
      9 => array(
        'state' => 'Passenger',
        'weight' => 6,
        'sysid' => 0,
        'status' => 1,
        'name' => 'Reservation State',
      ),
      10 => array(
        'state' => 'Completed',
        'weight' => 10,
        'sysid' => 0,
        'status' => 1,
        'name' => 'Reservation State',
      ),
    ),
    'transitions' => array(
      0 => array(
        'roles' => 'workflow_features_author_name',
        'state' => '(creation)',
        'target_state' => 'Awaiting Accountant ',
      ),
      1 => array(
        'roles' => 'workflow_features_author_name',
        'state' => 'Awaiting Balance Confirmation',
        'target_state' => 'Requester Sent new Confirmation',
      ),
      2 => array(
        'roles' => 'accounting',
        'state' => 'Balance Hours',
        'target_state' => 'Awaiting Balance Confirmation',
      ),
      3 => array(
        'roles' => 'workflow_features_author_name',
        'state' => 'Awaiting Requester',
        'target_state' => 'Reservation Admin',
      ),
      4 => array(
        'roles' => 'accounting',
        'state' => 'Awaiting Accountant ',
        'target_state' => 'Awaiting Requester',
      ),
      5 => array(
        'roles' => 'accounting',
        'state' => 'Requester Sent new Confirmation',
        'target_state' => 'Completed Reservation',
      ),
      6 => array(
        'roles' => 'Admin_site',
        'state' => 'Supervisor',
        'target_state' => 'Passenger',
      ),
      7 => array(
        'roles' => 'accounting',
        'state' => 'Reservation Admin',
        'target_state' => 'Supervisor',
      ),
      8 => array(
        'roles' => 'Admin_site',
        'state' => 'Passenger',
        'target_state' => 'Completed',
      ),
      9 => array(
        'roles' => 'authenticated user',
        'state' => 'Completed',
        'target_state' => 'Passenger',
      ),
      10 => array(
        'roles' => 'accounting,Admin_site',
        'state' => 'Completed',
        'target_state' => 'Balance Hours',
      ),
      11 => array(
        'roles' => 'Admin_site',
        'state' => 'Completed',
        'target_state' => 'Completed Reservation',
      ),
    ),
    'node_types' => array(
      0 => 'cartest',
    ),
  );

  return $workflows;
}
