<?php
/**
 * @file
 * reserve_car_features.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function reserve_car_features_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-care-reservation.
  $menus['menu-care-reservation'] = array(
    'menu_name' => 'menu-care-reservation',
    'title' => 'Care reservation',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Care reservation');

  return $menus;
}
