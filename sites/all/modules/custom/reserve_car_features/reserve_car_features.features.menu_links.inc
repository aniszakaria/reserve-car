<?php
/**
 * @file
 * reserve_car_features.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function reserve_car_features_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-care-reservation_administration-menu:<front>.
  $menu_links['menu-care-reservation_administration-menu:<front>'] = array(
    'menu_name' => 'menu-care-reservation',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Administration menu',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-care-reservation_administration-menu:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: menu-care-reservation_cost-summary:summary_cost_dept.
  $menu_links['menu-care-reservation_cost-summary:summary_cost_dept'] = array(
    'menu_name' => 'menu-care-reservation',
    'link_path' => 'summary_cost_dept',
    'router_path' => 'summary_cost_dept',
    'link_title' => 'Cost summary',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'menu-care-reservation_cost-summary:summary_cost_dept',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'parent_identifier' => 'menu-care-reservation_reports:<front>',
  );
  // Exported menu link: menu-care-reservation_create-reservation:node/add/cartest.
  $menu_links['menu-care-reservation_create-reservation:node/add/cartest'] = array(
    'menu_name' => 'menu-care-reservation',
    'link_path' => 'node/add/cartest',
    'router_path' => 'node/add/cartest',
    'link_title' => 'Create Reservation',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-care-reservation_create-reservation:node/add/cartest',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-care-reservation_department-summary:departmentsum.
  $menu_links['menu-care-reservation_department-summary:departmentsum'] = array(
    'menu_name' => 'menu-care-reservation',
    'link_path' => 'departmentsum',
    'router_path' => 'departmentsum',
    'link_title' => 'Department Summary',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'menu-care-reservation_department-summary:departmentsum',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
    'parent_identifier' => 'menu-care-reservation_reports:<front>',
  );
  // Exported menu link: menu-care-reservation_finalizing-reservations-feedback:Workflowfinal.
  $menu_links['menu-care-reservation_finalizing-reservations-feedback:Workflowfinal'] = array(
    'menu_name' => 'menu-care-reservation',
    'link_path' => 'Workflowfinal',
    'router_path' => 'Workflowfinal',
    'link_title' => 'Finalizing Reservations (Feedback)',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'menu-care-reservation_finalizing-reservations-feedback:Workflowfinal',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-care-reservation_administration-menu:<front>',
  );
  // Exported menu link: menu-care-reservation_full-reservation-summary:allsummary.
  $menu_links['menu-care-reservation_full-reservation-summary:allsummary'] = array(
    'menu_name' => 'menu-care-reservation',
    'link_path' => 'allsummary',
    'router_path' => 'allsummary',
    'link_title' => 'Full Reservation Summary',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'menu-care-reservation_full-reservation-summary:allsummary',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'customized' => 1,
    'parent_identifier' => 'menu-care-reservation_reports:<front>',
  );
  // Exported menu link: menu-care-reservation_logout:user/logout.
  $menu_links['menu-care-reservation_logout:user/logout'] = array(
    'menu_name' => 'menu-care-reservation',
    'link_path' => 'user/logout',
    'router_path' => 'user/logout',
    'link_title' => 'Logout',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-care-reservation_logout:user/logout',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'customized' => 1,
  );
  // Exported menu link: menu-care-reservation_my-reservations:reserve.
  $menu_links['menu-care-reservation_my-reservations:reserve'] = array(
    'menu_name' => 'menu-care-reservation',
    'link_path' => 'reserve',
    'router_path' => 'reserve',
    'link_title' => 'My Reservations',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'menu-care-reservation_my-reservations:reserve',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: menu-care-reservation_overtime:report_overtime.
  $menu_links['menu-care-reservation_overtime:report_overtime'] = array(
    'menu_name' => 'menu-care-reservation',
    'link_path' => 'report_overtime',
    'router_path' => 'report_overtime',
    'link_title' => 'Overtime',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'menu-care-reservation_overtime:report_overtime',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-care-reservation_reports:<front>',
  );
  // Exported menu link: menu-care-reservation_pending-requests:workflow-pending.
  $menu_links['menu-care-reservation_pending-requests:workflow-pending'] = array(
    'menu_name' => 'menu-care-reservation',
    'link_path' => 'workflow-pending',
    'router_path' => 'workflow-pending',
    'link_title' => 'Pending requests',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'menu-care-reservation_pending-requests:workflow-pending',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'menu-care-reservation_administration-menu:<front>',
  );
  // Exported menu link: menu-care-reservation_reports:<front>.
  $menu_links['menu-care-reservation_reports:<front>'] = array(
    'menu_name' => 'menu-care-reservation',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Reports',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-care-reservation_reports:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -45,
    'customized' => 1,
  );
  // Exported menu link: menu-care-reservation_reservations-date:summarydate.
  $menu_links['menu-care-reservation_reservations-date:summarydate'] = array(
    'menu_name' => 'menu-care-reservation',
    'link_path' => 'summarydate',
    'router_path' => 'summarydate',
    'link_title' => 'Reservations Date',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'menu-care-reservation_reservations-date:summarydate',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -43,
    'customized' => 1,
    'parent_identifier' => 'menu-care-reservation_reports:<front>',
  );
  // Exported menu link: menu-care-reservation_supervisor-admininstation:supervisor.
  $menu_links['menu-care-reservation_supervisor-admininstation:supervisor'] = array(
    'menu_name' => 'menu-care-reservation',
    'link_path' => 'supervisor',
    'router_path' => 'supervisor',
    'link_title' => 'Supervisor Admininstation',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'menu-care-reservation_supervisor-admininstation:supervisor',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-care-reservation_administration-menu:<front>',
  );
  // Exported menu link: menu-care-reservation_total-cost:total_cost.
  $menu_links['menu-care-reservation_total-cost:total_cost'] = array(
    'menu_name' => 'menu-care-reservation',
    'link_path' => 'total_cost',
    'router_path' => 'total_cost',
    'link_title' => 'Total cost',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'menu-care-reservation_total-cost:total_cost',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-care-reservation_reports:<front>',
  );
  // Exported menu link: menu-care-reservation_total-orders:total_orders.
  $menu_links['menu-care-reservation_total-orders:total_orders'] = array(
    'menu_name' => 'menu-care-reservation',
    'link_path' => 'total_orders',
    'router_path' => 'total_orders',
    'link_title' => 'Total orders',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'menu-care-reservation_total-orders:total_orders',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'menu-care-reservation_reports:<front>',
  );
  // Exported menu link: menu-care-reservation_weekend-orders:total_weekend_hours.
  $menu_links['menu-care-reservation_weekend-orders:total_weekend_hours'] = array(
    'menu_name' => 'menu-care-reservation',
    'link_path' => 'total_weekend_hours',
    'router_path' => 'total_weekend_hours',
    'link_title' => 'Weekend orders',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'menu-care-reservation_weekend-orders:total_weekend_hours',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'menu-care-reservation_reports:<front>',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Administration menu');
  t('Cost summary');
  t('Create Reservation');
  t('Department Summary');
  t('Finalizing Reservations (Feedback)');
  t('Full Reservation Summary');
  t('Logout');
  t('My Reservations');
  t('Overtime');
  t('Pending requests');
  t('Reports');
  t('Reservations Date');
  t('Supervisor Admininstation');
  t('Total cost');
  t('Total orders');
  t('Weekend orders');

  return $menu_links;
}
