<?php
/**
 * @file
 * reserve_car_features.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function reserve_car_features_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_arrivalarr|node|cartest|default';
  $field_group->group_name = 'group_arrivalarr';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'cartest';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Arrival',
    'weight' => '23',
    'children' => array(
      0 => 'field_addressarr',
      1 => 'field_airbagsarr',
      2 => 'field_airflightnoarr',
      3 => 'field_airlinearr',
      4 => 'field_airpersonsarr',
      5 => 'field_arrivaldate',
      6 => 'field_landmarksarr',
      7 => 'field_terminal',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_arrivalarr|node|cartest|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_arrivalarr|node|cartest|form';
  $field_group->group_name = 'group_arrivalarr';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'cartest';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Arrival',
    'weight' => '23',
    'children' => array(
      0 => 'field_addressarr',
      1 => 'field_airbagsarr',
      2 => 'field_airflightnoarr',
      3 => 'field_airlinearr',
      4 => 'field_airpersonsarr',
      5 => 'field_arrivaldate',
      6 => 'field_landmarksarr',
      7 => 'field_terminal',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_arrivalarr|node|cartest|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_arrivalarr|node|cartest|teaser';
  $field_group->group_name = 'group_arrivalarr';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'cartest';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Arrival',
    'weight' => '23',
    'children' => array(
      0 => 'field_addressarr',
      1 => 'field_airbagsarr',
      2 => 'field_airflightnoarr',
      3 => 'field_airlinearr',
      4 => 'field_airpersonsarr',
      5 => 'field_arrivaldate',
      6 => 'field_landmarksarr',
      7 => 'field_terminal',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_arrivalarr|node|cartest|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_departure|node|cartest|default';
  $field_group->group_name = 'group_departure';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'cartest';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Departure',
    'weight' => '21',
    'children' => array(
      0 => 'field_addressdrop',
      1 => 'field_airbags',
      2 => 'field_airflightno',
      3 => 'field_airlinearrdep',
      4 => 'field_airpersons',
      5 => 'field_departuredate',
      6 => 'field_landmark2',
      7 => 'field_terminalarrdep',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_departure|node|cartest|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_departure|node|cartest|form';
  $field_group->group_name = 'group_departure';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'cartest';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Departure',
    'weight' => '21',
    'children' => array(
      0 => 'field_addressdrop',
      1 => 'field_airbags',
      2 => 'field_airflightno',
      3 => 'field_airlinearrdep',
      4 => 'field_airpersons',
      5 => 'field_departuredate',
      6 => 'field_landmark2',
      7 => 'field_terminalarrdep',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_departure|node|cartest|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_departure|node|cartest|teaser';
  $field_group->group_name = 'group_departure';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'cartest';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Departure',
    'weight' => '21',
    'children' => array(
      0 => 'field_addressdrop',
      1 => 'field_airbags',
      2 => 'field_airflightno',
      3 => 'field_airlinearrdep',
      4 => 'field_airpersons',
      5 => 'field_departuredate',
      6 => 'field_landmark2',
      7 => 'field_terminalarrdep',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_departure|node|cartest|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dinfo|node|cartest|default';
  $field_group->group_name = 'group_dinfo';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'cartest';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Payment Details',
    'weight' => '28',
    'children' => array(
      0 => 'field_amount',
      1 => 'field_bank_receipt',
      2 => 'field_brept',
      3 => 'field_costcenter',
      4 => 'field_filefund',
      5 => 'field_fundreservation',
      6 => 'field_gl',
      7 => 'field_optwbscostcenter',
      8 => 'field_uploaddocu',
      9 => 'field_wbs',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_dinfo|node|cartest|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dinfo|node|cartest|form';
  $field_group->group_name = 'group_dinfo';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'cartest';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Payment Details',
    'weight' => '28',
    'children' => array(
      0 => 'field_amount',
      1 => 'field_bank_receipt',
      2 => 'field_brept',
      3 => 'field_costcenter',
      4 => 'field_filefund',
      5 => 'field_fundreservation',
      6 => 'field_gl',
      7 => 'field_optwbscostcenter',
      8 => 'field_uploaddocu',
      9 => 'field_wbs',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_dinfo|node|cartest|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dinfo|node|cartest|teaser';
  $field_group->group_name = 'group_dinfo';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'cartest';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Payment Details',
    'weight' => '28',
    'children' => array(
      0 => 'field_amount',
      1 => 'field_bank_receipt',
      2 => 'field_brept',
      3 => 'field_costcenter',
      4 => 'field_filefund',
      5 => 'field_fundreservation',
      6 => 'field_gl',
      7 => 'field_optwbscostcenter',
      8 => 'field_uploaddocu',
      9 => 'field_wbs',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_dinfo|node|cartest|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_requester_details|node|cartest|default';
  $field_group->group_name = 'group_requester_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'cartest';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Requester Details',
    'weight' => '20',
    'children' => array(
      0 => 'field_department',
      1 => 'field_email',
      2 => 'field_extensioninteger',
      3 => 'field_extensioninteger2',
      4 => 'field_mobilephoneinteger',
      5 => 'field_mobilephoneinteger2',
      6 => 'field_myselfsomeone',
      7 => 'field_namerequired',
      8 => 'field_requesterlastname',
      9 => 'field_requestername',
      10 => 'field_requiredext',
      11 => 'field_studentid',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_requester_details|node|cartest|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_requester_details|node|cartest|form';
  $field_group->group_name = 'group_requester_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'cartest';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Requester Details',
    'weight' => '20',
    'children' => array(
      0 => 'field_department',
      1 => 'field_email',
      2 => 'field_extensioninteger',
      3 => 'field_extensioninteger2',
      4 => 'field_mobilephoneinteger',
      5 => 'field_mobilephoneinteger2',
      6 => 'field_myselfsomeone',
      7 => 'field_namerequired',
      8 => 'field_requesterlastname',
      9 => 'field_requestername',
      10 => 'field_requiredext',
      11 => 'field_studentid',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_requester_details|node|cartest|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_requester_details|node|cartest|teaser';
  $field_group->group_name = 'group_requester_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'cartest';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Requester Details',
    'weight' => '20',
    'children' => array(
      0 => 'field_department',
      1 => 'field_email',
      2 => 'field_extensioninteger',
      3 => 'field_extensioninteger2',
      4 => 'field_mobilephoneinteger',
      5 => 'field_mobilephoneinteger2',
      6 => 'field_myselfsomeone',
      7 => 'field_namerequired',
      8 => 'field_requesterlastname',
      9 => 'field_requestername',
      10 => 'field_requiredext',
      11 => 'field_studentid',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_requester_details|node|cartest|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_reserve|node|cartest|default';
  $field_group->group_name = 'group_reserve';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'cartest';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Reservation Details',
    'weight' => '24',
    'children' => array(
      0 => 'field_datedroppick',
      1 => 'field_departuredate2',
      2 => 'field_detailsmarkup',
      3 => 'field_district',
      4 => 'field_dropoff',
      5 => 'field_finaldestination',
      6 => 'field_governorate',
      7 => 'field_landmark',
      8 => 'field_pickup',
      9 => 'field_pickupchanges',
      10 => 'field_pointuppoint',
      11 => 'field_reserveroundtrip',
      12 => 'field_roundtrip',
      13 => 'field_route',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_reserve|node|cartest|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_reserve|node|cartest|form';
  $field_group->group_name = 'group_reserve';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'cartest';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Reservation Details',
    'weight' => '24',
    'children' => array(
      0 => 'field_datedroppick',
      1 => 'field_departuredate2',
      2 => 'field_detailsmarkup',
      3 => 'field_district',
      4 => 'field_dropoff',
      5 => 'field_finaldestination',
      6 => 'field_governorate',
      7 => 'field_landmark',
      8 => 'field_pickup',
      9 => 'field_pickupchanges',
      10 => 'field_pointuppoint',
      11 => 'field_reserveroundtrip',
      12 => 'field_roundtrip',
      13 => 'field_route',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_reserve|node|cartest|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_reserve|node|cartest|teaser';
  $field_group->group_name = 'group_reserve';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'cartest';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Reservation Details',
    'weight' => '24',
    'children' => array(
      0 => 'field_datedroppick',
      1 => 'field_departuredate2',
      2 => 'field_detailsmarkup',
      3 => 'field_district',
      4 => 'field_dropoff',
      5 => 'field_finaldestination',
      6 => 'field_governorate',
      7 => 'field_landmark',
      8 => 'field_pickup',
      9 => 'field_pickupchanges',
      10 => 'field_pointuppoint',
      11 => 'field_reserveroundtrip',
      12 => 'field_roundtrip',
      13 => 'field_route',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_reserve|node|cartest|teaser'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Arrival');
  t('Departure');
  t('Payment Details');
  t('Requester Details');
  t('Reservation Details');

  return $field_groups;
}
