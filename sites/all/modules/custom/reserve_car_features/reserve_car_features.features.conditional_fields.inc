<?php
/**
 * @file
 * reserve_car_features.features.conditional_fields.inc
 */

/**
 * Implements hook_conditional_fields_default_fields().
 */
function reserve_car_features_conditional_fields_default_fields() {
  $items = array();

  $items["node:cartest:0"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_airportoption',
    'dependee' => 'field_reqtype',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Airport',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:1"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_bank_receipt',
    'dependee' => 'field_business_private_op',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Private',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:2"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_brept',
    'dependee' => 'field_business_private_op',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Private',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:3"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_bus',
    'dependee' => 'field_transportation_noful',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Mini Bus',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:4"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_bus_type2',
    'dependee' => 'field_transportation1',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Mini Bus',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:5"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_car_type2',
    'dependee' => 'field_transportation1',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Car',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:6"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_cartype',
    'dependee' => 'field_transportation_noful',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Car',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:7"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_costcenter',
    'dependee' => 'field_business_private_op',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Business',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:8"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_costcenter',
    'dependee' => 'field_optwbscostcenter',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Cost Center',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:9"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_datedroppick',
    'dependee' => 'field_roundtrip',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Yes',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:10"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_detailsmarkup',
    'dependee' => 'field_roundtrip',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Yes',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:11"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_dropoff',
    'dependee' => 'field_roundtrip',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Yes',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:12"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_extensioninteger2',
    'dependee' => 'field_myselfsomeone',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Someone else',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:13"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_filefund',
    'dependee' => 'field_business_private_op',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Business',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:14"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_filefund',
    'dependee' => 'field_optwbscostcenter',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Cost Center',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:15"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_fundreservation',
    'dependee' => 'field_business_private_op',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Business',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:16"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_gl',
    'dependee' => 'field_business_private_op',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Business',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:17"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_microbus',
    'dependee' => 'field_transportation_noful',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Micro-bus',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:18"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_microbus_type2',
    'dependee' => 'field_transportation1',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Micro-bus',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:19"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_mobilephoneinteger2',
    'dependee' => 'field_myselfsomeone',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Someone else',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:20"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_namerequired',
    'dependee' => 'field_myselfsomeone',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Someone else',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:21"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_optwbscostcenter',
    'dependee' => 'field_business_private_op',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Business',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:22"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_pickup',
    'dependee' => 'field_pickupchanges',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Yes',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:23"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_pickup',
    'dependee' => 'field_roundtrip',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Yes',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:24"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_pickupchanges',
    'dependee' => 'field_roundtrip',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Yes',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:25"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_repeattime',
    'dependee' => 'field_repeat',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Yes',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:26"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_requiredext',
    'dependee' => 'field_business_private_op',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Private',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:27"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_reserveroundtrip',
    'dependee' => 'field_roundtrip',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Yes',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:28"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_transportation1',
    'dependee' => 'field_business_private_op',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Business',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:29"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_transportation_noful',
    'dependee' => 'field_business_private_op',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Private',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:30"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_trucks',
    'dependee' => 'field_transportation1',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Half Truck',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:31"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_uploaddocu',
    'dependee' => 'field_business_private_op',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Business',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:32"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_uploaddocu',
    'dependee' => 'field_optwbscostcenter',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'WBS',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:33"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_wbs',
    'dependee' => 'field_business_private_op',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'Business',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  $items["node:cartest:34"] = array(
    'entity' => 'node',
    'bundle' => 'cartest',
    'dependent' => 'field_wbs',
    'dependee' => 'field_optwbscostcenter',
    'options' => array(
      'values_set' => 2,
      'values' => array(
        0 => 'WBS',
      ),
      'effect' => 'slide',
      'effect_options' => array(
        'speed' => 400,
      ),
      'element_view' => array(
        2 => 0,
        5 => 5,
      ),
      'element_edit' => array(
        1 => 0,
        2 => 2,
        3 => 3,
      ),
      'selector' => '',
      'state' => 'visible',
      'condition' => 'value',
      'value' => array(),
      'value_form' => array(),
      'element_view_per_role' => 0,
      'element_view_roles' => array(),
      'element_edit_per_role' => 0,
      'element_edit_roles' => array(),
      'grouping' => 'AND',
    ),
  );

  return $items;
}
