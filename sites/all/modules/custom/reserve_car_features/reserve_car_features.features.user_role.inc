<?php
/**
 * @file
 * reserve_car_features.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function reserve_car_features_user_default_roles() {
  $roles = array();

  // Exported role: Admin_site.
  $roles['Admin_site'] = array(
    'name' => 'Admin_site',
    'weight' => 0,
  );

  // Exported role: accounting.
  $roles['accounting'] = array(
    'name' => 'accounting',
    'weight' => 0,
  );

  // Exported role: anonymous user.
  $roles['anonymous user'] = array(
    'name' => 'anonymous user',
    'weight' => 0,
  );

  // Exported role: authenticated user.
  $roles['authenticated user'] = array(
    'name' => 'authenticated user',
    'weight' => 0,
  );

  return $roles;
}
