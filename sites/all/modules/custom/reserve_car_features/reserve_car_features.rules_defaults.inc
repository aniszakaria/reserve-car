<?php
/**
 * @file
 * reserve_car_features.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function reserve_car_features_default_rules_configuration() {
  $items = array();
  $items['rules_complete_with_no_balance'] = entity_import('rules_config', '{ "rules_complete_with_no_balance" : {
      "LABEL" : "complete with no balance",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "workflow_rules", "rules", "php" ],
      "ON" : { "node_update--cartest" : { "bundle" : "cartest" } },
      "IF" : [
        { "workflow_check_state" : { "node" : [ "node" ], "workflow_state" : { "value" : { "83" : "83" } } } },
        { "data_is" : {
            "data" : [ "node:field-realcost" ],
            "op" : "\\u003C",
            "value" : [ "node:field-hours" ]
          }
        }
      ],
      "DO" : [
        { "workflow_rules_set_state" : {
            "node" : [ "node" ],
            "workflow_state" : { "value" : { "83" : "83" } },
            "workflow_comment" : "Action set [node:title] to [node:workflow-current-state-name]."
          }
        },
        { "mail" : {
            "to" : "carpool@aucegypt.edu,[node:field-email]",
            "subject" : "CR#[node:field_datenow-yy][node:field_datenow-mm]\\u003C?php echo substr([node:field_datenow-timestamp],-4); ?\\u003E confirmed successfully",
            "message" : "Dear [node:field_requestername],\\r\\n\\r\\n     The following reservation has been confirmed successfully. \\r\\n\\r\\nPersonal Information\\r\\n     - First Name: [node:field_requestername]\\r\\n     - Last Name: [node:field_requesterlastname]\\t\\r\\n     - AUC ID: [node:field_studentid]\\r\\n     - Department: [node:field_department]\\r\\n\\r\\nReservation Information\\r\\n\\r\\n     - Request Type: [node:field_reqtype] [node:field_airportoption]\\r\\n     - Business \\/ Private: [node:field_business_private_op]\\r\\n\\r\\n     - Pick up Point: [node:field_pointuppoint][node:field_addressdrop]\\r\\n     - Destination: [node:field_finaldestination][node:field_addressarr]\\r\\n     - Date: [node:field_departuredate2-datetime][node:field_departuredate-datetime][node:field_arrivaldate-datetime]\\r\\n \\r\\nYou can go back and view your reservation by clicking on this link: [node:node-url]\\r\\n\\r\\nSincerely,\\r\\nCar Reservation Team",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_completed_with_balance'] = entity_import('rules_config', '{ "rules_completed_with_balance" : {
      "LABEL" : "Completed with balance",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "workflow_rules", "php" ],
      "ON" : { "workflow_state_changed" : [] },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "cartest" : "cartest" } } } },
        { "workflow_check_transition" : {
            "node" : [ "node" ],
            "old_state" : { "value" : { "84" : "84" } },
            "new_state" : { "value" : { "83" : "83" } }
          }
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : [ "node:field-email" ],
            "subject" : "CR#[node:field_datenow-yy][node:field_datenow-mm]\\u003C?php echo substr([node:field_datenow-timestamp],-4); ?\\u003E confirmed successfully",
            "message" : "Dear [node:field_requestername-formatted],\\r\\n\\r\\n     The following reservation has been confirmed successfully. \\r\\n\\r\\nPersonal Information\\r\\n     - First Name: [node:field_requestername-formatted]\\r\\n     - Last Name: [node:field_requesterlastname-formatted]\\t\\r\\n     - AUC ID: [node:field_studentid-formatted]\\r\\n     - Department: [node:field_department-formatted]\\r\\n\\r\\nReservation Information\\r\\n\\r\\n     - Request Type: [node:field_reqtype-formatted] [node:field_airportoption-formatted]\\r\\n     - Business \\/ Private: [node:field_business_private_op-formatted]\\r\\n\\r\\n     - Pick up Point: [node:field_pointuppoint-formatted][node:field_addressdrop-formatted]\\r\\n     - Destination: [node:field_finaldestination-formatted][node:field_addressarr-formatted]\\r\\n     - Date: [node:field_departuredate2-datetime][node:field_departuredate-datetime][node:field_arrivaldate-datetime]\\r\\n \\r\\nYou can go back and view your reservation by clicking on this link: [node:node-url]\\r\\n\\r\\nSincerely,\\r\\nCar Reservation Team",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_delete_reservation'] = entity_import('rules_config', '{ "rules_delete_reservation" : {
      "LABEL" : "Delete Reservation",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "OWNER" : "rules",
      "REQUIRES" : [ "php", "rules" ],
      "ON" : { "node_delete" : [] },
      "DO" : [
        { "mail" : {
            "to" : [ "node:author:mail" ],
            "subject" : "CR#[node:field_datenow-yy][node:field_datenow-mm]\\u003C?php echo substr([node:field_datenow-timestamp], -4); ?\\u003E has been deleted",
            "message" : "Dear,\\r\\n\\r\\n     The following reservation (Reference Number is: CR#[node:field_datenow-yy][node:field_datenow-mm]\\u003C?php echo substr([node:field_datenow-timestamp], -4); ?\\u003E has been deleted and no longer accessible.\\r\\n\\r\\nSincerely,\\r\\nCar Reservation Team",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_difference_in_hours_actual_hrs_estimated_hrs_'] = entity_import('rules_config', '{ "rules_difference_in_hours_actual_hrs_estimated_hrs_" : {
      "LABEL" : "Editing rule difference in hours (Actual Hrs \\u003E Estimated Hrs)",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "workflow_rules", "rules", "php" ],
      "ON" : { "node_update--cartest" : { "bundle" : "cartest" } },
      "IF" : [
        { "workflow_check_state" : { "node" : [ "node" ], "workflow_state" : { "value" : { "77" : "77" } } } },
        { "data_is" : {
            "data" : [ "node:field-realcost" ],
            "op" : "\\u003E",
            "value" : [ "node:field-hours" ]
          }
        }
      ],
      "DO" : [
        { "workflow_rules_set_state" : {
            "node" : [ "node" ],
            "workflow_state" : { "value" : { "81" : "81" } },
            "workflow_comment" : "Action set [node:title] to [node:workflow-current-state-name]."
          }
        },
        { "mail" : {
            "to" : "carpool@aucegypt.edu",
            "subject" : "CR#[node:field_datenow-yy][node:field_datenow-mm]\\u003C?php echo substr([node:field_datenow-timestamp],-4); ?\\u003E balance difference",
            "message" : "Dear,\\r\\n\\r\\n     The following reservation CR:[node:field_datenow-yy][node:field_datenow-mm]\\u003C?php echo substr([node:field_datenow-timestamp], -4);?\\u003E has a balance difference , please click on the link here [node:node-url] to follow up.\\r\\n\\r\\nPersonal Information\\r\\n     - First Name: [node:field_requestername-formatted]\\r\\n     - Last Name: [node:field_requesterlastname-formatted]\\t\\r\\n     - AUC ID: [node:field_studentid-formatted]\\r\\n     - Department: [node:field_department-formatted]\\r\\n\\r\\nReservation Information\\r\\n     - Request Type: [node:field_reqtype-formatted] [node:field_airportoption-formatted]\\r\\n     - Business \\/ Private: [node:field_business_private_op-formatted]\\r\\n\\r\\n     - Pick up Point: [node:field_pointuppoint-formatted][node:field_addressdrop-formatted]\\r\\n     - Destination: [node:field_finaldestination-formatted][node:field_addressarr-formatted]\\r\\n     - Date: [node:field_departuredate2-datetime][node:field_departuredate-datetime][node:field_arrivaldate-datetime]\\r\\n \\r\\nYou can go back and view your reservation by clicking on this link: [node:node-url]\\/edit\\r\\n\\r\\nSincerely,\\r\\nCar Reservation Team.",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_example_1'] = entity_import('rules_config', '{ "rules_example_1" : {
      "LABEL" : "Example rule: When viewing an unpublished page, publish it.",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_view" : [] },
      "IF" : [
        { "NOT node_is_published" : { "node" : [ "node" ] } },
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : [ "page" ] } } }
      ],
      "DO" : [ { "node_publish" : { "node" : [ "node" ] } } ]
    }
  }');
  $items['rules_send_account_mail_with_confirmed_uploaded_balance'] = entity_import('rules_config', '{ "rules_send_account_mail_with_confirmed_uploaded_balance" : {
      "LABEL" : "Send account mail with CONFIRMED uploaded Balance",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "workflow_rules", "php" ],
      "ON" : { "workflow_state_changed" : [] },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "cartest" : "cartest" } } } },
        { "workflow_check_transition" : {
            "node" : [ "node" ],
            "old_state" : { "value" : { "82" : "82" } },
            "new_state" : { "value" : { "84" : "84" } }
          }
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : "carpool@aucegypt.edu, anis.zakaria@aucegypt.edu",
            "subject" : "CR#[node:field_datenow-yy][node:field_datenow-mm]\\u003C?php echo substr([node:field_datenow-timestamp], -4); ?\\u003E Bal. Corfimation Sent",
            "message" : "Dear,\\r\\n\\r\\n     The following reservation CR:[node:field_datenow-yy][node:field_datenow-mm]\\u003C?php echo substr([node:field_datenow-timestamp], -4);?\\u003E has confirmed the balance difference and updated his funds reservation, please click on the link here [node:node-url] to follow up with the new funds reservation.\\r\\n\\r\\nPersonal Information\\r\\n     - First Name: [node:field_requestername-formatted]\\r\\n     - Last Name: [node:field_requesterlastname-formatted]\\t\\r\\n     - AUC ID: [node:field_studentid-formatted]\\r\\n     - Department: [node:field_department-formatted]\\r\\n\\r\\nReservation Information\\r\\n     - Request Type: [node:field_reqtype-formatted] [node:field_airportoption-formatted]\\r\\n     - Business \\/ Private: [node:field_business_private_op-formatted]\\r\\n\\r\\n     - Pick up Point: [node:field_pointuppoint-formatted][node:field_addressdrop-formatted]\\r\\n     - Destination: [node:field_finaldestination-formatted][node:field_addressarr-formatted]\\r\\n     - Date: [node:field_departuredate2-datetime][node:field_departuredate-datetime][node:field_arrivaldate-datetime]\\r\\n \\r\\nYou can go back and view your reservation by clicking on this link: [node:node-url]\\/edit\\r\\n\\r\\nSincerely,\\r\\nCar Reservation Team.",
            "from" : ""
          }
        }
      ]
    }
  }');
  $items['rules_send_client_mail_with_uploaded_balance'] = entity_import('rules_config', '{ "rules_send_client_mail_with_uploaded_balance" : {
      "LABEL" : "Send Client mail with uploaded Balance",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "workflow_rules", "php" ],
      "ON" : { "workflow_state_changed" : [] },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "cartest" : "cartest" } } } },
        { "workflow_check_transition" : {
            "node" : [ "node" ],
            "old_state" : { "value" : { "81" : "81" } },
            "new_state" : { "value" : { "82" : "82" } }
          }
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : [ "node:field-email" ],
            "subject" : "CR#[node:field_datenow-yy][node:field_datenow-mm]\\u003C?php echo substr([node:field_datenow-timestamp], -4); ?\\u003E needs attention",
            "message" : "Dear [node:field_requestername-formatted],\\r\\n\\r\\n     The following reservation CR:[node:field_datenow-yy][node:field_datenow-mm]\\u003C?php echo substr([node:field_datenow-timestamp], -4);?\\u003E needs your attention, please click on the link here [node:node-url] to access your application to fill in the required information\\r\\n\\r\\nRequired Information\\r\\n     - Estimated cost:  [node:field_cost-formatted]\\r\\n     - Amount needed to be paid due to Balance estimation differences: [node:field_extra-formatted]\\r\\n     - Please update your financial payment details and upload your fund reservation or Bank receipt document.\\r\\n\\r\\nPersonal Information\\r\\n     - First Name: [node:field_requestername-formatted]\\r\\n     - Last Name: [node:field_requesterlastname-formatted]\\t\\r\\n     - AUC ID: [node:field_studentid-formatted]\\r\\n     - Department: [node:field_department-formatted]\\r\\n\\r\\nReservation Information\\r\\n     - Request Type: [node:field_reqtype-formatted] [node:field_airportoption-formatted]\\r\\n     - Business \\/ Private: [node:field_business_private_op-formatted]\\r\\n\\r\\n     - Pick up Point: [node:field_pointuppoint-formatted][node:field_addressdrop-formatted]\\r\\n     - Destination: [node:field_finaldestination-formatted][node:field_addressarr-formatted]\\r\\n     - Date: [node:field_departuredate2-datetime][node:field_departuredate-datetime][node:field_arrivaldate-datetime]\\r\\n \\r\\nYou can go back and view your reservation by clicking on this link: [node:node-url]\\/edit\\r\\n\\r\\nSincerely,\\r\\nCar Reservation Team.",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_send_e_mail_from_accounting_and_supervisor'] = entity_import('rules_config', '{ "rules_send_e_mail_from_accounting_and_supervisor" : {
      "LABEL" : "Send e-mail from accounting to supervisor",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "workflow_rules", "php" ],
      "ON" : { "workflow_state_changed" : [] },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "cartest" : "cartest" } } } },
        { "workflow_check_transition" : {
            "node" : [ "node" ],
            "old_state" : { "value" : { "73" : "73" } },
            "new_state" : { "value" : { "75" : "75" } }
          }
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : "carpool@aucegypt.edu",
            "subject" : "CR#[node:field_datenow-yy][node:field_datenow-mm]\\u003C?php echo substr([node:field_datenow-timestamp], -4); ?\\u003E needs attention",
            "message" : "Dear,\\r\\n\\r\\n     The following reservation need your attention. Please click on this link [node:node-url]\\/edit to complete the form with the necessary data.\\r\\n\\r\\nPlease assign driver and car.\\r\\n\\r\\nPersonal Information\\r\\n     - First Name: [node:field_requestername-formatted]\\r\\n     - Last Name: [node:field_requesterlastname-formatted]\\t\\r\\n     - AUC ID: [node:field_studentid-formatted]\\r\\n     - Department: [node:field_department-formatted]\\r\\n\\r\\nReservation Information\\r\\n\\r\\n     - Request Type: [node:field_reqtype-formatted] [node:field_airportoption-formatted]\\r\\n     - Business \\/ Private: [node:field_business_private_op-formatted]\\r\\n\\r\\n     - Pick up Point: [node:field_pointuppoint-formatted][node:field_addressdrop-formatted]\\r\\n     - Destination: [node:field_finaldestination-formatted][node:field_addressarr-formatted]\\r\\n     - Date: [node:field_departuredate2-datetime][node:field_departuredate-datetime][node:field_arrivaldate-datetime]\\r\\n\\r\\nSincerely,\\r\\nCar Reservation Team",
            "from" : ""
          }
        }
      ]
    }
  }');
  $items['rules_send_e_mail_from_accounting_to_client_all_data_completed_'] = entity_import('rules_config', '{ "rules_send_e_mail_from_accounting_to_client_all_data_completed_" : {
      "LABEL" : "Send e-mail from Supervisor to Client (All Data completed)",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "workflow_rules", "php" ],
      "ON" : { "workflow_state_changed" : [] },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "cartest" : "cartest" } } } },
        { "workflow_check_transition" : {
            "node" : [ "node" ],
            "old_state" : { "value" : { "75" : "75" } },
            "new_state" : { "value" : { "76" : "76" } }
          }
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : "[node:field_email-formatted],carpool@aucegypt.edu",
            "subject" : "CR#[node:field_datenow-yy][node:field_datenow-mm]\\u003C?php echo substr([node:field_datenow-timestamp],-4); ?\\u003E reserved successfully",
            "message" : "Dear [node:field_requestername-formatted],\\r\\n\\r\\n     The following reservation has been reserved successfully. \\r\\n\\r\\nPersonal Information\\r\\n     - First Name: [node:field_requestername-formatted]\\r\\n     - Last Name: [node:field_requesterlastname-formatted]\\t\\r\\n     - AUC ID: [node:field_studentid-formatted]\\r\\n     - Department: [node:field_department-formatted]\\r\\n\\r\\nReservation Information\\r\\n\\r\\n     - Request Type: [node:field_reqtype-formatted] [node:field_airportoption-formatted]\\r\\n     - Business \\/ Private: [node:field_business_private_op-formatted]\\r\\n\\r\\n     - Pick up Point: [node:field_pointuppoint-formatted][node:field_addressdrop-formatted]\\r\\n     - Destination: [node:field_finaldestination-formatted][node:field_addressarr-formatted]\\r\\n     - Date: [node:field_departuredate2-datetime][node:field_departuredate-datetime][node:field_arrivaldate-datetime]\\r\\n \\r\\n     - Driver Name and Mobile: [node:field_driver-formatted]\\r\\nYou can go back and view your reservation by clicking on this link: [node:node-url]\\r\\n\\r\\nSincerely,\\r\\nCar Reservation Team",
            "from" : ""
          }
        }
      ]
    }
  }');
  $items['rules_send_e_mail_to_accounting_after_finishing_funds_process'] = entity_import('rules_config', '{ "rules_send_e_mail_to_accounting_after_finishing_funds_process" : {
      "LABEL" : "Send e-mail to accounting after finishing funds process",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "workflow_rules", "php" ],
      "ON" : { "workflow_state_changed" : [] },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "cartest" : "cartest" } } } },
        { "workflow_check_transition" : {
            "node" : [ "node" ],
            "old_state" : { "value" : { "79" : "79" } },
            "new_state" : { "value" : { "73" : "73" } }
          }
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : [ "node:field-email" ],
            "subject" : "CR#[node:field_datenow-yy][node:field_datenow-mm]\\u003C?php echo substr([node:field_datenow-timestamp], -4); ?\\u003E Update",
            "message" : "Dear [node:field_requestername-formatted],\\r\\n\\r\\n     The following reservation is being processed. You will be receiving an e-mail with the confirmation details. Your reference number is: CR#[node:field_datenow-yy][node:field_datenow-mm]\\u003C?php echo substr([node:field_datenow-timestamp], -4); ?\\u003E. Please save this number for all correspondence regarding this reservation.\\r\\n\\r\\nPersonal Information\\r\\n     - First Name: [node:field_requestername-formatted]\\r\\n     - Last Name: [node:field_requesterlastname-formatted]\\t\\r\\n     - AUC ID: [node:field_studentid-formatted]\\r\\n     - Department: [node:field_department-formatted]\\r\\n\\r\\nReservation Information\\r\\n     - Request Type: [node:field_reqtype-formatted] [node:field_airportoption-formatted]\\r\\n     - Business \\/ Private: [node:field_business_private_op-formatted]\\r\\n\\r\\n     - Pick up Point: [node:field_pointuppoint-formatted][node:field_addressdrop-formatted]\\r\\n     - Destination: [node:field_finaldestination-formatted][node:field_addressarr-formatted]\\r\\n     - Date: [node:field_departuredate2-datetime][node:field_departuredate-datetime][node:field_arrivaldate-datetime]\\r\\n \\r\\nYou can go back and view your reservation by clicking on this link: [node:node-url]\\r\\n\\r\\nSincerely,\\r\\nCar Reservation Team",
            "language" : [ "" ]
          }
        },
        { "mail" : {
            "to" : "ehabhelal@aucegypt.edu",
            "subject" : "CR#[node:field_datenow-yy][node:field_datenow-mm]\\u003C?php echo substr([node:field_datenow-timestamp], -4); ?\\u003E has been Updated",
            "message" : "Dear,\\r\\n\\r\\n     The following reservation has been updated with the necessary fund details. Please click on this link [node:node-url]\\/edit to view the attached financial documents. Once confirmed please click on submit to allow the supervisor to assign a vehicle.\\r\\n\\r\\nPersonal Information\\r\\n     - First Name: [node:field_requestername-formatted]\\r\\n     - Last Name: [node:field_requesterlastname-formatted]\\t\\r\\n     - AUC ID: [node:field_studentid-formatted]\\r\\n     - Department: [node:field_department-formatted]\\r\\n\\r\\nReservation Information\\r\\n     - Request Type: [node:field_reqtype-formatted] [node:field_airportoption-formatted]\\r\\n     - Business \\/ Private: [node:field_business_private_op-formatted]\\r\\n\\r\\n     - Pick up Point: [node:field_pointuppoint-formatted][node:field_addressdrop-formatted]\\r\\n     - Destination: [node:field_finaldestination-formatted][node:field_addressarr-formatted]\\r\\n     - Date: [node:field_departuredate2-datetime][node:field_departuredate-datetime][node:field_arrivaldate-datetime]\\r\\n \\r\\nYou can go back and view your reservation by clicking on this link: [node:node-url]\\r\\n\\r\\nSincerely,\\r\\nCar Reservation Team",
            "from" : ""
          }
        }
      ]
    }
  }');
  $items['rules_send_e_mail_to_accounting_and_client_someone_else_'] = entity_import('rules_config', '{ "rules_send_e_mail_to_accounting_and_client_someone_else_" : {
      "LABEL" : "Send e-mail to accounting and client (Someone ELSE)",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "workflow_rules", "php" ],
      "ON" : { "workflow_state_changed" : [] },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "cartest" : "cartest" } } } },
        { "workflow_check_transition" : {
            "node" : [ "node" ],
            "old_state" : { "value" : { "ANY" : "ANY" } },
            "new_state" : { "value" : { "78" : "78" } }
          }
        },
        { "data_is" : { "data" : [ "node:field-myselfsomeone" ], "value" : "Someone else" } }
      ],
      "DO" : [
        { "mail" : {
            "to" : [ "node:field-email" ],
            "subject" : "CR#[node:field_datenow-yy][node:fieldd_atenow-mm]\\u003C?php echo substr([node:field_datenow-timestamp], -4); ?\\u003E has been created",
            "message" : "Dear [node:field_requestername-formatted],\\r\\n\\r\\n     The following reservation is being processed. You will be receiving an e-mail with the confirmation details. Your reference number is: CR#[node:field_datenow-yy][node:field_datenow-mm]\\u003C?php echo substr([node:field_datenow-timestamp], -4); ?\\u003E. Please save this number for all correspondence regarding this reservation.\\r\\n\\r\\nReservation made by:\\r\\n     - First Name: [node:field_requestername-formatted]\\r\\n     - Last Name: [node:field_requesterlastname-formatted]\\t\\r\\n     - AUC ID: [node:field_studentid-formatted]\\r\\n     - Department: [node:field_department-formatted]\\r\\n\\r\\nReserved for: \\r\\n     - Full Name: [node:field_namerequired-formatted]\\r\\n     - Mobile Number: [node:field_mobilerequired-formatted]\\t\\r\\n\\r\\nReservation Information\\r\\n     - Request Type: [node:field_reqtype-formatted] [node:field_airportoption-formatted]\\r\\n     - Business \\/ Private: [node:field_business_private_op-formatted]\\r\\n\\r\\nDeparture\\r\\n     - Pick up Point: [node:field_pointuppoint-formatted][node:field_addressdrop-formatted]\\r\\n     - Date: [node:field_departuredate2-datetime][node:field_departuredate-datetime]\\r\\n     - Address: [node:field_addressdrop-formatted]\\r\\n\\r\\nArrival\\r\\n     - Destination: [node:field_finaldestination-formatted][node:field_addressarr-formatted]\\r\\n     - Date: [node:field_arrivaldate-datetime]\\r\\n     - Address: [node:field_addressarr-formatted]\\r\\n \\r\\nYou can go back and view your reservation by clicking on this link: [node:node-url]\\r\\nYou can view printable version by clicking on this link: https:\\/\\/reserve-car.aucegypt.edu\\/print\\/[node:nid]\\r\\nSincerely,\\r\\nCar Reservation Team",
            "language" : [ "" ]
          }
        },
        { "mail" : {
            "to" : "carpool@aucegypt.edu",
            "subject" : "CR#[node:field_datenow-yy][node:field_datenow-mm]\\u003C?php echo substr([node:field_datenow-timestamp], -4); ?\\u003E has been created",
            "message" : "Dear,\\r\\n\\r\\n     The following reservation has been created. Please click on this link [node:node-url]\\/edit to complete the form with the necessary data.\\r\\n\\r\\nPlease assign the number of hours and cost per hour for the below reservation\\r\\n\\r\\nReservation Information:\\r\\n\\r\\nReservation made by:\\r\\n     - First Name: [node:field_requestername-formatted]\\r\\n     - Last Name: [node:field_requesterlastname-formatted]\\t\\r\\n     - AUC ID: [node:field_studentid-formatted]\\r\\n     - Department: [node:field_department-formatted]\\r\\n\\r\\nReserved for: \\r\\n     - Full Name: [node:field_namerequired-formatted]\\r\\n     - Mobile Number: [node:field_mobilerequired-formatted]\\r\\n\\r\\n     - Request Type: [node:field_reqtype-formatted] [node:field_airportoption-formatted]\\r\\n     - Business \\/ Private: [node:field_business_private_op-formatted]\\r\\n\\r\\n     - Pick up Point: [node:field_pointuppoint-formatted][node:field_addressdrop-formatted]\\r\\n     - Destination: [node:field_finaldestination-formatted][node:field_addressarr-formatted]\\r\\n     - Date: [node:field_departuredate2-datetime][node:field_departuredate-datetime][node:field_arrivaldate-datetime]\\r\\n \\r\\nYou can go back and view your reservation by clicking on this link: [node:node-url]\\r\\n\\r\\nSincerely,\\r\\nCar Reservation Team",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_send_e_mail_w_no_wbs_amount'] = entity_import('rules_config', '{ "rules_send_e_mail_w_no_wbs_amount" : {
      "LABEL" : "Send e-mail w\\/ NO WBS \\u0026 Amount",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "workflow_rules", "php" ],
      "ON" : { "workflow_state_changed" : [] },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "cartest" : "cartest" } } } },
        { "workflow_check_transition" : {
            "node" : [ "node" ],
            "old_state" : { "value" : { "78" : "78" } },
            "new_state" : { "value" : { "79" : "79" } }
          }
        },
        { "data_is" : { "data" : [ "node:field-reqtype" ], "value" : "Regular" } }
      ],
      "DO" : [
        { "mail" : {
            "to" : [ "node:field-email" ],
            "subject" : "CR#[node:field_datenow-yy][node:field_datenow-mm]\\u003C?php echo substr([node:field_datenow-timestamp], -4); ?\\u003E Update",
            "message" : "Dear [node:field_requestername-formatted],\\r\\n\\r\\n     Your request needs to be updated. \\r\\n\\r\\nThe estimated total cost is [node:field_finalcost-raw]EGP\\r\\n\\r\\nPlease upload your financial documents.\\r\\n\\r\\nYou can go back and view your reservation by clicking on this link: [node:node-url]\\/edit\\r\\nand you can view printable version by clicking on this link: https:\\/\\/reserve-car.aucegypt.edu\\/print\\/[node:nid]\\r\\n\\r\\nPersonal Information\\r\\n     - First Name: [node:field_requestername-formatted]\\r\\n     - Last Name: [node:field_requesterlastname-formatted]\\t\\r\\n     - AUC ID: [node:field_studentid-formatted]\\r\\n     - Department: [node:field_department-formatted]\\r\\n\\r\\nReservation Information\\r\\n     - Request Type: [node:field_reqtype-formatted] [node:field_airportoption-formatted]\\r\\n     - Business \\/ Private: [node:field_business_private_op-formatted]\\r\\n\\r\\n     - Pick up Point: [node:field_pointuppoint-formatted][node:field_addressdrop-formatted]\\r\\n     - Destination: [node:field_finaldestination-formatted][node:field_addressarr-formatted]\\r\\n     - Date: [node:field_departuredate2-datetime][node:field_departuredate-datetime][node:field_arrivaldate-datetime]\\r\\n \\r\\nSincerely,\\r\\nCar Reservation Team",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_send_e_mail_w_wbs_amount'] = entity_import('rules_config', '{ "rules_send_e_mail_w_wbs_amount" : {
      "LABEL" : "Send e-mail w\\/WBS \\u0026 Amount",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "workflow_rules", "php" ],
      "ON" : { "workflow_state_changed" : [] },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "cartest" : "cartest" } } } },
        { "workflow_check_transition" : {
            "node" : [ "node" ],
            "old_state" : { "value" : { "78" : "78" } },
            "new_state" : { "value" : { "79" : "79" } }
          }
        },
        { "data_is" : { "data" : [ "node:field-reqtype" ], "value" : "Airport" } }
      ],
      "DO" : [
        { "mail" : {
            "to" : [ "node:field-email" ],
            "subject" : "Car Reservation CR#[node:field_datenow-yy][node:field_datenow-mm][node:nid] Update",
            "message" : "Dear [node:field_requestername-formatted],\\r\\n\\r\\n     Your request needs to be updated. You can go back and view your reservation by clicking on this link: [node:node-url]\\/edit\\r\\nand you can view printable version by clicking on this link: https:\\/\\/reserve-car.aucegypt.edu\\/print\\/[node:nid]\\r\\n\\r\\n\\r\\nPlease upload\\/update your financial details.\\r\\n\\r\\nPersonal Information\\r\\n     - First Name: [node:field_requestername-formatted]\\r\\n     - Last Name: [node:field_requesterlastname-formatted]\\t\\r\\n     - AUC ID: [node:field_studentid-formatted]\\r\\n     - Department: [node:field_department-formatted]\\r\\n\\r\\nReservation Information\\r\\n     - Request Type: [node:field_reqtype-formatted] [node:field_airportoption-formatted]\\r\\n     - Business \\/ Private: [node:field_business_private_op-formatted]\\r\\n\\r\\n     - Pick up Point: [node:field_pointuppoint-formatted][node:field_addressdrop-formatted]\\r\\n     - Destination: [node:field_finaldestination-formatted][node:field_addressarr-formatted]\\r\\n     - Date: [node:field_departuredate2-datetime][node:field_departuredate-datetime][node:field_arrivaldate-datetime]\\r\\n \\r\\nSincerely,\\r\\nCar Reservation Team",
            "language" : [ "" ]
          }
        },
        { "mail" : {
            "to" : "amira_hg@aucegypt.edu",
            "subject" : "CR#[node:field_datenow-yy][node:field_datenow-mm]\\u003C?php echo substr([node:field_datenow-timestamp], -4); ?\\u003E - WBS",
            "message" : "Dear Mr\\/Ms.......\\r\\n\\r\\nOn [node:field_deptdate-date] -  [node:field_requesterlastname-formatted], [node:field_requestername-formatted] ([node:author-name-raw])  has used the following WBS Account : [node:field_wbs-formatted] to create a car reservation with a total amount of  [node:field_finalcost-raw]EGP.\\r\\n\\r\\nPlease confirm this request by sending your approval to carpool@aucegypt.edu\\r\\n\\r\\nSincerely,\\r\\nCar Reservation Team",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_set_1'] = entity_import('rules_config', '{ "rules_set_1" : {
      "LABEL" : "Example: Empty rule set working with content",
      "PLUGIN" : "rule set",
      "OWNER" : "rules",
      "USES VARIABLES" : { "node" : { "type" : "node", "label" : "Content" } },
      "RULES" : []
    }
  }');
  $items['rules_test'] = entity_import('rules_config', '{ "rules_test" : {
      "LABEL" : "Send e-mail to accounting and client",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "workflow_rules", "php" ],
      "ON" : { "workflow_state_changed" : [] },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "cartest" : "cartest" } } } },
        { "workflow_check_transition" : {
            "node" : [ "node" ],
            "old_state" : { "value" : { "ANY" : "ANY" } },
            "new_state" : { "value" : { "78" : "78" } }
          }
        },
        { "data_is" : { "data" : [ "node:field-myselfsomeone" ], "value" : "Myself" } }
      ],
      "DO" : [
        { "mail" : {
            "to" : [ "node:field-email" ],
            "subject" : "CR#[node:field_datenow-yy][node:field_datenow-mm]\\u003C?php echo substr([node:field_datenow-timestamp], -4); ?\\u003E has been created",
            "message" : "Dear [node:field_requestername-formatted],\\r\\n\\r\\n     The following reservation is being processed. You will be receiving an e-mail with the confirmation details. Your reference number is: CR:[node:field_datenow-yy][node:field_datenow-mm]\\u003C?php echo substr([node:field_datenow-timestamp], -4);?\\u003E\\r\\n\\r\\nPlease save this number for all correspondence regarding this reservation.\\r\\n\\r\\nPersonal Information\\r\\n     - First Name: [node:field_requestername-formatted]\\r\\n     - Last Name: [node:field_requesterlastname-formatted]\\t\\r\\n     - AUC ID: [node:field_studentid-formatted]\\r\\n     - Department: [node:field_department-formatted]\\r\\n\\r\\nReservation Information\\r\\n     - Request Type: [node:field_reqtype-formatted] [node:field_airportoption-formatted]\\r\\n     - Business \\/ Private: [node:field_business_private_op-formatted]\\r\\n\\r\\nDeparture\\r\\n     - Pick up Point: [node:field_pointuppoint-formatted][node:field_addressdrop-formatted]\\r\\n     - Date: [node:field_departuredate2-datetime][node:field_departuredate-datetime]\\r\\n     - Address: [node:field_addressdrop-formatted]\\r\\n\\r\\nArrival\\r\\n     - Destination: [node:field_finaldestination-formatted][node:field_addressarr-formatted]\\r\\n     - Date: [node:field_arrivaldate-datetime]\\r\\n     - Address: [node:field_addressarr-formatted]\\r\\n \\r\\nYou can go back and view your reservation by clicking on this link: [node:node-url]\\r\\n\\r\\nSincerely,\\r\\nCar Reservation Team.",
            "language" : [ "" ]
          }
        },
        { "mail" : {
            "to" : "carpool@aucegypt.edu",
            "subject" : "CR#[node:field_datenow-yy][node:field_datenow-mm]\\u003C?php echo substr([node:field_datenow-timestamp], -4); ?\\u003E has been created",
            "message" : "Dear,\\r\\n\\r\\n     The following reservation has been created. Please click on this link [node:node-url]\\/edit to complete the form with the necessary data.\\r\\n\\r\\nPlease assign the number of hours and cost per hour for the following reservation\\r\\n\\r\\nReservation Details:\\r\\n\\r\\nPersonal Information\\r\\n     - First Name: [node:field_requestername-formatted]\\r\\n     - Last Name: [node:field_requesterlastname-formatted]\\t\\r\\n     - AUC ID: [node:field_studentid-formatted]\\r\\n     - Department: [node:field_department-formatted]\\r\\n\\r\\n     - Request Type: [node:field_reqtype-formatted] [node:field_airportoption-formatted]\\r\\n     - Business \\/ Private: [node:field_business_private_op-formatted]\\r\\n\\r\\n     - Pick up Point: [node:field_pointuppoint-formatted][node:field_addressdrop-formatted]\\r\\n     - Destination: [node:field_finaldestination-formatted][node:field_addressarr-formatted]\\r\\n     - Date: [node:field_departuredate2-datetime][node:field_departuredate-datetime][node:field_arrivaldate-datetime]\\r\\n \\r\\nYou can go back and view your reservation by clicking on this link: [node:node-url]\\/edit\\r\\n\\r\\nSincerely,\\r\\nCar Reservation Team",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_update_email_notification'] = entity_import('rules_config', '{ "rules_update_email_notification" : {
      "LABEL" : "UPDATE email notification",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "php" ],
      "ON" : { "node_update--cartest" : { "bundle" : "cartest" } },
      "DO" : [
        { "mail" : {
            "to" : "carpool@aucegypt.edu, [node:author-mail]",
            "subject" : "CR#[node:field_datenow-yy][node:field_datenow-mm]\\u003C?php echo substr([node:field_datenow-timestamp], -4); ?\\u003E has been updated",
            "message" : "Dear,\\r\\n\\r\\n     The following reservation has been updated: CR#[node:field_datenow-yy][node:field_datenow-mm]\\u003C?php echo substr([node:field_datenow-timestamp], -4); ?\\u003E. \\r\\n\\r\\nReservation Details:\\r\\n\\r\\nPersonal Information\\r\\n     - First Name: [node:field_requestername-formatted]\\r\\n     - Last Name: [node:field_requesterlastname-formatted]\\t\\r\\n     - AUC ID: [node:field_studentid-formatted]\\r\\n     - Department: [node:field_department-formatted]\\r\\n\\r\\nReservation Information\\r\\n     - Request Type: [node:field_reqtype-formatted] [node:field_airportoption-formatted]\\r\\n     - Business \\/ Private: [node:field_business_private_op-formatted]\\r\\n\\r\\n     - Pick up Point: [node:field_pointuppoint-formatted][node:field_addressdrop-formatted]\\r\\n     - Destination: [node:field_finaldestination-formatted][node:field_addressarr-formatted]\\r\\n     - Date: [node:field_departuredate2-datetime][node:field_departuredate-datetime][node:field_arrivaldate-datetime]\\r\\n \\r\\nYou can go back and view your reservation by clicking on this link: [node:node-url]\\r\\nYou can view printable version by clicking on this link: https:\\/\\/reserve-car.aucegypt.edu\\/print\\/[node:nid]\\r\\n\\r\\nSincerely,\\r\\nCar Reservation Team",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  return $items;
}
